CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

REQUIREMENTS
------------

RECOMMENDED MODULES
-------------------

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
 https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Go to your content type and add a new field of type Entity revisions,
   Paragraphs.
 * Allow only one Paragraph to each node.
 * On the field edit screen, you can add instructions, and choose the Story
   bundle.
 * Adjust your form display, placing the field where you want it.
 * Add the field into the Manage display tab. Set the Label to hidden.
 * Start creating content!


TROUBLESHOOTING
---------------

MAINTAINERS
-----------

Current maintainers:
 * [thejimbirch](https://www.drupal.org/u/thejimbirch)
 * [banoodle](https://www.drupal.org/u/banoodle)

This project has been sponsored by:
 * [Kanopi studios](https://www.drupal.org/kanopi-studios)
