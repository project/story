(function ($, Drupal) {
  Drupal.behaviors.jump_menu = {
    attach: function (context, settings) {

      $(document).ready(function() {
        $('#jump-menu-inner a[href*=\\#]').bind('click', function(e) {
          e.preventDefault(); // Prevent hard jump, the default behavior.

          var target = $(this).attr("href"); // Set the target as variable.

          // Perform animated scrolling by getting top-position of target-element and set it as scroll target.
          $('html, body').stop().animate({
            scrollTop: $(target).offset().top
          }, 600, function() {
            location.hash = target; // Attach the hash (#jumptarget) to the url.
          });

          // Active chapter classes triggered by click.
          $('a.active').removeClass('active');
          $(this).addClass('active');

          return false;
        });
      });

      $(window).scroll(function() {
        var fromTop = $(window).scrollTop();
        var scrollDistance = fromTop - 200;

        // Active chapter classes triggered by scroll.
        $('.paragraph--type--st-chapter').each(function(i) {
          if ($(this).position().top <= scrollDistance) {

            $('#jump-menu-inner a.active').removeClass('active');
            $('#jump-menu-inner a').eq(i).addClass('active');
          }
        });
      }).scroll();

    }
  };

  Drupal.behaviors.chapter_animation = {
    attach: function (context, settings) {

      $(document).ready(function() {
        AOS.init();
      });

    }
  };

  Drupal.behaviors.scroll_sticky = {
    attach: function (context, settings) {

      // Initialize jquery.menu-anchor.
      $(window).scroll(function() {
        var scrollDistance = $(window).scrollTop();
        if ($('#jump-menu').length) {
          var stickySidebar = $('#chapters').offset().top;

          if(scrollDistance >= stickySidebar) {
            $('#jump-menu').addClass('sticky');
            $('#chapters').addClass('scrolled');
          } else {
            $('#jump-menu.sticky').removeClass('sticky');
            $('#chapters.scrolled').removeClass('scrolled');
          }
        }
      }).scroll();

    }
  };

})(jQuery, Drupal);


